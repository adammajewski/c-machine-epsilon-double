/* 
http://en.wikipedia.org/wiki/Machine_epsilon
The following C program does not actually determine the machine epsilon;
rather, it determines a number within a factor of two (one order of magnitude) 
of the true machine epsilon, using a linear search.

---

The difference between 1 and the least value greater than 1 that is representable in the given floating-point type, b1-p.
-------------------------------
http://stackoverflow.com/questions/1566198/how-to-portably-get-dbl-epsilon-in-c-c

gcc m.c -Wall

./a.out
*/


#include <stdio.h>
#include <float.h> // DBL_EPSILON
 
 int main()
 {
    double epsilon = 1.0;
 
    printf( "epsilon;  1 + epsilon\n" );
    
    do 
     {
       printf( "%G\t%.20f\n", epsilon, (1.0 + epsilon) );
       epsilon /= 2.0f;
     }
    // If next epsilon yields 1, then break
    while ((1.0 + (epsilon/2.0)) != 1.0); // 

    // because current epsilon is the calculated machine epsilon.
    printf( "\nCalculated Machine epsilon: %G\n", epsilon );
    
    
    //check value from float.h , Steve Jessop
    if ((1.0 + DBL_EPSILON) != 1.0 
        && 
       (1.0 + DBL_EPSILON/2) == 1.0)
     printf("DBL_EPSILON = %g \n", DBL_EPSILON);
     else printf("DBL_EPSILON is not good !!! \n");


    return 0;
 }

